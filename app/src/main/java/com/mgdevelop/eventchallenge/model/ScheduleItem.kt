package com.mgdevelop.eventchallenge.model

data class ScheduleItem(
    override val id: String,
    override val title: String,
    override val subtitle: String,
    override val date: String,
    override val imageUrl: String
) : BaseAdapterItem(id = id, title = title, subtitle = subtitle, date = date, imageUrl = imageUrl)