package com.mgdevelop.eventchallenge.ui.itemadapter

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mgdevelop.eventchallenge.R
import com.mgdevelop.eventchallenge.databinding.AdapterItemBinding
import com.mgdevelop.eventchallenge.model.BaseAdapterItem
import com.mgdevelop.eventchallenge.model.EventItem
import com.mgdevelop.eventchallenge.utils.Const.Companion.EmptyString
import java.text.SimpleDateFormat
import java.util.*


class BaseAdapter(private val onClick: (String) -> Unit) :
    RecyclerView.Adapter<BaseAdapter.BaseViewHolder>() {

    private var items = mutableListOf<BaseAdapterItem>()
    private val parseDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    private val formatDateAndTime = SimpleDateFormat("HH:mm", Locale.US)
    private val formatDate = SimpleDateFormat("dd.MM.yyyy", Locale.US)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = AdapterItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BaseViewHolder(binding, parseDate, formatDateAndTime, formatDate)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.set(items[position])
        if (items[position] is EventItem) {
            holder.setOnClick(onClick, (items[position] as EventItem).videoUrl)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(items: List<BaseAdapterItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    class BaseViewHolder(
        val binding: AdapterItemBinding,
        val parseDate: SimpleDateFormat,
        val formatDateAndTime: SimpleDateFormat,
        val formatDate: SimpleDateFormat
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun set(item: BaseAdapterItem) {
            binding.apply {
                title.text = item.title
                subtitle.text = item.subtitle
                date.text = formatDate(item.date)
                Glide
                    .with(root.context)
                    .load(item.imageUrl)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .into(cover)
            }
        }

        fun setOnClick(onClick: (String) -> Unit, videoUrl: String) {
            binding.root.setOnClickListener { onClick.invoke(videoUrl) }
        }

        private fun formatDate(date: String): String = parseDate.parse(date)?.run {
            when {
                isToday(this) -> {
                    "Today, ${formatDateAndTime.format(this)}"
                }
                isYesterday(this) -> {
                    "Yesterday, ${formatDateAndTime.format(this)}"
                }
                else -> {
                    formatDate.format(this)
                }
            }
        } ?: EmptyString


        private fun isToday(d: Date): Boolean = DateUtils.isToday(d.time)
        private fun isYesterday(d: Date): Boolean =
            DateUtils.isToday(d.time + DateUtils.DAY_IN_MILLIS)

    }
}
