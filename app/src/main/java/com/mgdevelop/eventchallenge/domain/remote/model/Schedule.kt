package com.mgdevelop.eventchallenge.domain.remote.model

data class Schedule(
    val id: String,
    val title: String,
    val subtitle: String,
    val date: String,
    val imageUrl: String
)