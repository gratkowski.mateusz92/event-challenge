package com.mgdevelop.eventchallenge.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mgdevelop.eventchallenge.utils.DialogUtils
import com.mgdevelop.eventchallenge.utils.observeNotNull
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject


abstract class BaseActivity<VM : BaseViewModel> constructor(
    private val viewModelType: Class<VM>
) : AppCompatActivity() {

    @Inject
    lateinit var viewModel: VM
    val disposables by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeError()
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    open fun showError(message: String) {
        DialogUtils.showDefaultErrorDialog(this, message)
    }

    private fun observeError() {
        viewModel.errorState.observeNotNull(this) {
            when (it) {
                is BaseViewModel.BaseViewState.OnError -> {
                    showError(it.text)
                }
                BaseViewModel.BaseViewState.NoInternet -> DialogUtils.showNoInternetDialog(this)
            }
        }
    }
}
