package com.mgdevelop.eventchallenge.module

import android.content.Context
import com.mgdevelop.eventchallenge.domain.error.ErrorSourceInteractor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun errorSourceInteractor(@ApplicationContext context: Context) =
        ErrorSourceInteractor(context = context)
}