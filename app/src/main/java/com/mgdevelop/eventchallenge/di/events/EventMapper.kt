package com.mgdevelop.eventchallenge.di.events

import com.mgdevelop.eventchallenge.domain.remote.model.Event
import com.mgdevelop.eventchallenge.model.EventItem
import javax.inject.Inject

class EventMapper @Inject constructor() {
    fun mapResponse(response: List<Event>): List<EventItem> =
        response.map {
            EventItem(
                id = it.id,
                title = it.title,
                subtitle = it.subtitle,
                date = it.date,
                imageUrl = it.imageUrl,
                videoUrl = it.videoUrl
            )
        }
}