package com.mgdevelop.eventchallenge.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mgdevelop.eventchallenge.domain.error.ErrorSourceInteractor
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseViewModel() : ViewModel() {

    private val _errorState = MutableLiveData<BaseViewState>()
    val errorState: LiveData<BaseViewState> = _errorState

    @Inject
    lateinit var errorInteractor: ErrorSourceInteractor

    protected val disposables = CompositeDisposable()

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }

    fun handleResponse(data: Any?, error: Throwable?, onClick: (Any) -> Unit) {
        if (error != null) {
            onError(error)
        } else {
            data?.run {
                onClick.invoke(this)
            }
        }
    }

    fun onError(error: Throwable?) {
        _errorState.value = when (error) {
            else -> BaseViewState.OnError(
                error?.message ?: errorInteractor.getDefaultMessage(),
                error
            )
        }
    }

    sealed class BaseViewState {
        data class OnError(val text: String, val throwable: Throwable? = null) : BaseViewState()
        object NoInternet : BaseViewState()
    }
}
