package com.mgdevelop.eventchallenge.ui.videoplayback

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.util.Util
import com.mgdevelop.eventchallenge.databinding.ActivityVideoPlaybackBinding
import com.mgdevelop.eventchallenge.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

private const val VIDEO_URL = "video_url"

@AndroidEntryPoint
class VideoPlaybackActivity :
    BaseActivity<VideoPlaybackViewModel>(VideoPlaybackViewModel::class.java) {

    private lateinit var binding: ActivityVideoPlaybackBinding
    private var mPlayer: ExoPlayer? = null

    private var videoURL =
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityVideoPlaybackBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getStringExtra(VIDEO_URL)?.run {
            videoURL = this
        }
    }

    private fun initPlayer() {
        mPlayer = ExoPlayer.Builder(this).build().apply {
            binding.exoPlayer.player = this
            playWhenReady = true
            setMediaSource(buildMediaSource())
            prepare()
        }
    }

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT >= 24) {
            initPlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        if (Util.SDK_INT < 24 || mPlayer == null) {
            initPlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT < 24) {
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT >= 24) {
            releasePlayer()
        }
    }

    private fun releasePlayer() {
        if (mPlayer == null) {
            return
        }
        mPlayer!!.release()
        mPlayer = null
    }

    private fun buildMediaSource(): MediaSource =
        ProgressiveMediaSource.Factory(DefaultHttpDataSource.Factory())
            .createMediaSource(MediaItem.fromUri(videoURL))
}

fun createVideoPlaybackIntent(context: Context, videoUrl: String) =
    Intent(context, VideoPlaybackActivity::class.java).apply {
        putExtra(VIDEO_URL, videoUrl)
    }