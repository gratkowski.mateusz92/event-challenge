package com.mgdevelop.eventchallenge.model

import com.mgdevelop.eventchallenge.utils.Const.Companion.EmptyString

data class EventItem(
    override val id: String,
    override val title: String,
    override val subtitle: String,
    override val date: String,
    override val imageUrl: String,
    val videoUrl: String = EmptyString
) : BaseAdapterItem(id = id, title = title, subtitle = subtitle, date = date, imageUrl = imageUrl)