package com.mgdevelop.eventchallenge.domain.remote

import com.mgdevelop.eventchallenge.domain.remote.model.Event
import com.mgdevelop.eventchallenge.domain.remote.model.Schedule
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface RemoteApi {

    @GET("getEvents")
    fun getEvents(): Single<List<Event>>

    @GET("getSchedule")
    fun getSchedule(): Single<List<Schedule>>
}