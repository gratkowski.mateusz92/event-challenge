package com.mgdevelop.eventchallenge.di.schedule

import com.mgdevelop.eventchallenge.domain.remote.model.Schedule
import com.mgdevelop.eventchallenge.model.ScheduleItem
import javax.inject.Inject

class ScheduleMapper @Inject constructor() {
    fun mapResponse(response: List<Schedule>): List<ScheduleItem> =
        response.map {
            ScheduleItem(
                id = it.id,
                title = it.title,
                subtitle = it.subtitle,
                date = it.date,
                imageUrl = it.imageUrl
            )
        }
}