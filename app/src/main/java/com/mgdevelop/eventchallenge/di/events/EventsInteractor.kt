package com.mgdevelop.eventchallenge.di.events

import com.mgdevelop.eventchallenge.model.EventItem
import io.reactivex.rxjava3.core.Single

interface EventsInteractor {
    fun getEvents(): Single<List<EventItem>>
}