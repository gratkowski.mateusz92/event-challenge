package com.mgdevelop.eventchallenge.module

import com.mgdevelop.eventchallenge.di.events.EventMapper
import com.mgdevelop.eventchallenge.di.events.EventsInteractor
import com.mgdevelop.eventchallenge.di.events.EventsInteractorImpl
import com.mgdevelop.eventchallenge.domain.CoreRepositories
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
class EventModule {

    @Provides
    fun provideEventsInteractor(
        coreRepositories: CoreRepositories,
        eventMapper: EventMapper
    ): EventsInteractor =
        EventsInteractorImpl(repositories = coreRepositories, eventMapper = eventMapper)
}