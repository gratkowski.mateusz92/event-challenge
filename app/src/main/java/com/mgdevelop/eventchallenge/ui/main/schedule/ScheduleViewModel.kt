package com.mgdevelop.eventchallenge.ui.main.schedule

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mgdevelop.eventchallenge.di.schedule.ScheduleInteractor
import com.mgdevelop.eventchallenge.model.ScheduleItem
import com.mgdevelop.eventchallenge.ui.base.BaseViewModel
import com.mgdevelop.eventchallenge.utils.plusAssign
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class ScheduleViewModel @Inject constructor(val scheduleInteractor: ScheduleInteractor) :
BaseViewModel() {

    private val _viewState = MutableLiveData<ScheduleViewState>()
    val viewState: LiveData<ScheduleViewState> = _viewState

    fun init() {
        disposables += scheduleInteractor.getSchedule()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { response, error ->
                handleResponse(response, error) {
                    _viewState.value = ScheduleViewState.SetEvents(response)
                }
            }
    }

    sealed class ScheduleViewState {
        data class SetEvents(val schedule : List<ScheduleItem>) : ScheduleViewState()
    }
}