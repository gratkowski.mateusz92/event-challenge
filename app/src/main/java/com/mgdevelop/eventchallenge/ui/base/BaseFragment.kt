package com.mgdevelop.eventchallenge.ui.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.mgdevelop.eventchallenge.utils.DialogUtils.Companion.showDefaultErrorDialog
import com.mgdevelop.eventchallenge.utils.DialogUtils.Companion.showNoInternetDialog
import com.mgdevelop.eventchallenge.utils.observeNotNull
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseFragment<VM : BaseViewModel> constructor(
    private val viewModelType: Class<VM>
) : Fragment() {

    @Inject
    lateinit var viewModel: VM
    val disposables by lazy { CompositeDisposable() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeError()
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    open fun showError(message: String) = context?.run {
        showDefaultErrorDialog(this, message)
    }

    private fun observeError() {
        viewModel.errorState.observeNotNull(this) {
            when (it) {
                is BaseViewModel.BaseViewState.OnError -> {
                    showError(it.text)
                }
                BaseViewModel.BaseViewState.NoInternet -> context?.run {
                    showNoInternetDialog(this)
                }
            }
        }
    }
}
