package com.mgdevelop.eventchallenge.di.schedule

import com.mgdevelop.eventchallenge.domain.CoreRepositories
import com.mgdevelop.eventchallenge.model.ScheduleItem
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ScheduleInteractorImp @Inject internal constructor(
    private val repositories: CoreRepositories,
    private val scheduleMapper: ScheduleMapper
) : ScheduleInteractor {

    override fun getSchedule(): Single<List<ScheduleItem>> =
        repositories.remoteSourceInteractor.getSchedule().map { scheduleMapper.mapResponse(it) }
}
