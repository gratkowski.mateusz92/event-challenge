package com.mgdevelop.eventchallenge.domain.remote

import com.mgdevelop.eventchallenge.domain.remote.model.Event
import com.mgdevelop.eventchallenge.domain.remote.model.Schedule
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class RemoteSourceInteractor @Inject constructor(
    private val remoteApi: RemoteApi
) {
    fun getEvents(): Single<List<Event>> = remoteApi.getEvents()

    fun getSchedule(): Single<List<Schedule>> = remoteApi.getSchedule()
}