package com.mgdevelop.eventchallenge.di.schedule

import com.mgdevelop.eventchallenge.model.ScheduleItem
import io.reactivex.rxjava3.core.Single

interface ScheduleInteractor {
    fun getSchedule(): Single<List<ScheduleItem>>
}