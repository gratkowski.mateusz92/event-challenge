package com.mgdevelop.eventchallenge.ui.main.schedule

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.mgdevelop.eventchallenge.databinding.FragmentScheduleBinding
import com.mgdevelop.eventchallenge.ui.base.BaseFragment
import com.mgdevelop.eventchallenge.ui.itemadapter.BaseAdapter
import com.mgdevelop.eventchallenge.ui.main.schedule.ScheduleViewModel.ScheduleViewState.SetEvents
import com.mgdevelop.eventchallenge.utils.observeNotNull
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ScheduleFragment : BaseFragment<ScheduleViewModel>(ScheduleViewModel::class.java) {

    private var _binding: FragmentScheduleBinding? = null
    private val binding get() = _binding!!

    val stationsAdapter by lazy {
        BaseAdapter{
            Log.i("CLICK", "NO")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentScheduleBinding.inflate(inflater, container, false)
        val root: View = binding.root
        prepareView()
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.init()
    }

    private fun prepareView() {
        binding.schedule.run {
            layoutManager = LinearLayoutManager(context)
            adapter = stationsAdapter
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.observeNotNull(this) {
            when (it) {
                is SetEvents -> {
                    stationsAdapter.setItems(it.schedule)
                    binding.progress.visibility = View.GONE
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}