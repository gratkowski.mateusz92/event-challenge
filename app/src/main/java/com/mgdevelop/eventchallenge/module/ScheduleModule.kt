package com.mgdevelop.eventchallenge.module

import com.mgdevelop.eventchallenge.di.schedule.ScheduleInteractor
import com.mgdevelop.eventchallenge.di.schedule.ScheduleInteractorImp
import com.mgdevelop.eventchallenge.di.schedule.ScheduleMapper
import com.mgdevelop.eventchallenge.domain.CoreRepositories
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class ScheduleModule {

    @Provides
    fun provideScheduleInteractor(
        coreRepositories: CoreRepositories,
        scheduleMapper: ScheduleMapper
    ): ScheduleInteractor =
        ScheduleInteractorImp(repositories = coreRepositories, scheduleMapper = scheduleMapper)
}