package com.mgdevelop.eventchallenge.domain.error

import android.content.Context
import com.mgdevelop.eventchallenge.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class ErrorSourceInteractor @Inject constructor(@ApplicationContext private val context : Context) {

    fun getDefaultMessage():String{
        return context.getString(R.string.default_error)
    }
}
