package com.mgdevelop.eventchallenge.model

import com.mgdevelop.eventchallenge.utils.Const.Companion.EmptyString

abstract class BaseAdapterItem(
    open val id: String = EmptyString,
    open val title: String = EmptyString,
    open val subtitle: String = EmptyString,
    open val date: String = EmptyString,
    open val imageUrl: String = EmptyString,
)