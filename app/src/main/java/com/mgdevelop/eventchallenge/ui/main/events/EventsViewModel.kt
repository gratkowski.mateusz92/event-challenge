package com.mgdevelop.eventchallenge.ui.main.events

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mgdevelop.eventchallenge.di.events.EventsInteractor
import com.mgdevelop.eventchallenge.model.EventItem
import com.mgdevelop.eventchallenge.ui.base.BaseViewModel
import com.mgdevelop.eventchallenge.utils.plusAssign
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class EventsViewModel @Inject constructor(val eventsInteractor: EventsInteractor) :
    BaseViewModel() {

    private val _viewState = MutableLiveData<DashboardViewState>()
    val viewState: LiveData<DashboardViewState> = _viewState

    fun init() {
        disposables += eventsInteractor.getEvents()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { response, error ->
                handleResponse(response, error) {
                   _viewState.value = DashboardViewState.SetEvents(response)
                }
            }
    }

    sealed class DashboardViewState {
        data class SetEvents(val tasks : List<EventItem>) : DashboardViewState()
    }
}