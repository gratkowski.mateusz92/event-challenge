package com.mgdevelop.eventchallenge.domain

import com.mgdevelop.eventchallenge.domain.remote.RemoteSourceInteractor
import javax.inject.Inject

class CoreRepositories @Inject constructor(val remoteSourceInteractor: RemoteSourceInteractor)