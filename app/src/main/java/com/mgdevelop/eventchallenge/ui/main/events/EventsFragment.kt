package com.mgdevelop.eventchallenge.ui.main.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.mgdevelop.eventchallenge.databinding.FragmentEventsBinding
import com.mgdevelop.eventchallenge.ui.base.BaseFragment
import com.mgdevelop.eventchallenge.ui.itemadapter.BaseAdapter
import com.mgdevelop.eventchallenge.ui.main.events.EventsViewModel.DashboardViewState.SetEvents
import com.mgdevelop.eventchallenge.ui.videoplayback.createVideoPlaybackIntent
import com.mgdevelop.eventchallenge.utils.observeNotNull
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EventsFragment : BaseFragment<EventsViewModel>(EventsViewModel::class.java) {

    private var _binding: FragmentEventsBinding? = null
    private val binding get() = _binding!!

    val stationsAdapter by lazy {
        BaseAdapter {
            goToVideoPlaybackActivity(videoUrl = it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEventsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        prepareView()
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.init()
    }

    private fun prepareView() {
        binding.events.run {
            layoutManager = LinearLayoutManager(context)
            adapter = stationsAdapter
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.observeNotNull(this) {
            when (it) {
                is SetEvents -> {
                    stationsAdapter.setItems(it.tasks)
                    binding.progress.visibility = GONE
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun goToVideoPlaybackActivity(videoUrl: String) = context?.run {
        startActivity(createVideoPlaybackIntent(this, videoUrl))
    }
}