package com.mgdevelop.eventchallenge.di.events

import com.mgdevelop.eventchallenge.domain.CoreRepositories
import com.mgdevelop.eventchallenge.model.EventItem
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class EventsInteractorImpl @Inject internal constructor(
    private val repositories: CoreRepositories,
    private val eventMapper: EventMapper
) : EventsInteractor {
    override fun getEvents(): Single<List<EventItem>> =
        repositories.remoteSourceInteractor.getEvents().map { eventMapper.mapResponse(it) }

}