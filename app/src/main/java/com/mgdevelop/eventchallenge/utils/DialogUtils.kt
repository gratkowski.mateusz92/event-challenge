package com.mgdevelop.eventchallenge.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.mgdevelop.eventchallenge.R


class DialogUtils {
    companion object {


        fun showNoInternetDialog(context: Context) {
            val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
            alertDialog.setMessage(context.getString(R.string.no_internet_connection))

            alertDialog.setButton(
                AlertDialog.BUTTON_POSITIVE,
                context.getString(R.string.ok)
            ) { dialog, which ->
                dialog.dismiss()
            }

            alertDialog.show()
        }

        fun showDefaultErrorDialog(context: Context, message: String) {
            val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle(context.getString(R.string.default_error))
            alertDialog.setMessage(message)

            alertDialog.setButton(
                AlertDialog.BUTTON_POSITIVE,
                context.getString(R.string.ok)
            ) { dialog, which ->
                dialog.dismiss()
            }

            alertDialog.show()
        }
    }
}